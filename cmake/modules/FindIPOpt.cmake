include(FindPackageHandleStandardArgs)

#find_package(PkgConfig REQUIRED)
#pkg_check_modules(IPOPT ipopt)

# some versions of Ipopt need the DL library
find_library(DL_LIBRARY dl)

find_path(IPOPT_INCLUDE_DIR
  NAMES "IpNLP.hpp"
  PATHS ${IPOPT_ROOT}
  PATH_SUFFIXES "include" "include/coin" "include/coin-or"
  NO_DEFAULT_PATH
)
find_path(IPOPT_INCLUDE_DIR
  NAMES "IpNLP.hpp"
  PATH_SUFFIXES "include" "include/coin" "include/coin-or"
)

find_library(IPOPT_LIBRARY
  NAMES ipopt
  PATHS ${IPOPT_ROOT}
  PATH_SUFFIXES "lib"
  NO_DEFAULT_PATH
)
find_library(IPOPT_LIBRARY
  NAMES ipopt
)

# If you want to want to use other linear solver
find_library(HSL_LIBRARY
  NAMES hsl coinhsl
  PATHS ${IPOPT_ROOT}
  PATH_SUFFIXES "lib"
  NO_DEFAULT_PATH
)
find_library(HSL_LIBRARY
  NAMES hsl coinhsl
)

find_package_handle_standard_args(hsl DEFAULT_MSG HSL_LIBRARY)
find_package_handle_standard_args(dl DEFAULT_MSG DL_LIBRARY)
find_package_handle_standard_args(IPOpt DEFAULT_MSG IPOPT_INCLUDE_DIR IPOPT_LIBRARY)

if(IPOPT_FOUND)
    set(HAVE_IPOPT ENABLE_IPOPT)
    set(DUNE_IPOPT_CFLAGS "-I${IPOPT_INCLUDE_DIR} -DENABLE_IPOPT=1 -DHAVE_CSTDDEF=1")
    set_property(GLOBAL APPEND PROPERTY ALL_PKG_FLAGS "-I${IPOPT_INCLUDE_DIR}")
    set(IPOPT_LIBRARY ${IPOPT_LIBRARY})
    if (HSL_FOUND)
        set (IPOPT_LIBRARY ${HSL_LIBRARY} ${IPOPT_LIBRARY})
    endif(HSL_FOUND)
    if (DL_FOUND)
        set (IPOPT_LIBRARY ${DL_LIBRARY} ${IPOPT_LIBRARY})
    endif(DL_FOUND)

    # register all related flags
    dune_register_package_flags(COMPILE_DEFINITIONS "ENABLE_IPOPT=1" "HAVE_CSTDDEF=1"
                                LIBRARIES "${IPOPT_LIBRARY}"
                                INCLUDE_DIRS "${IPOPT_INCLUDE_DIR}")
endif(IPOPT_FOUND)
