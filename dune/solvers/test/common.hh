// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_TESTS_COMMON_HH
#define DUNE_SOLVERS_TESTS_COMMON_HH

#include <random>

#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/matrixindexset.hh>

#include <dune/geometry/quadraturerules.hh>

#include <dune/localfunctions/lagrange/lagrangelfecache.hh>

#include <dune/grid/yaspgrid.hh>

#include <dune/grid/utility/structuredgridfactory.hh>

#if HAVE_DUNE_UGGRID
#include <dune/grid/uggrid.hh>
#endif

#if HAVE_DUNE_ALUGRID
#include <dune/alugrid/grid.hh>
#endif

#include <dune/matrix-vector/addtodiagonal.hh>

#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/norms/twonorm.hh>


template<class GridView, class Matrix>
void constructPQ1Pattern(const GridView& gridView, Matrix& matrix)
{
    static const int dim = GridView::Grid::dimension;

    typedef typename Dune::LagrangeLocalFiniteElementCache<double, double, dim, 1> FiniteElementCache;
    typedef typename FiniteElementCache::FiniteElementType FiniteElement;

    const auto& indexSet = gridView.indexSet();
    FiniteElementCache cache;

    int size = indexSet.size(dim);

    Dune::MatrixIndexSet indices(size, size);

    for (const auto& element : elements(gridView))
    {
        const FiniteElement& fe = cache.get(element.type());

        int localSize = fe.size();
        for (int i = 0; i < localSize; ++i)
        {
            int iGlobal = indexSet.subIndex(element, fe.localCoefficients().localKey(i).subEntity(), dim);
            for (int j = 0; j < localSize; ++j)
            {
                int jGlobal = indexSet.subIndex(element, fe.localCoefficients().localKey(j).subEntity(), dim);
                indices.add(iGlobal, jGlobal);
                indices.add(jGlobal, iGlobal);
            }
        }
    }
    indices.exportIdx(matrix);
}

template<class GridView, class Matrix>
void assemblePQ1Stiffness(const GridView& gridView, Matrix& matrix)
{
    static constexpr int dim = GridView::Grid::dimension;
    static constexpr int dimworld = GridView::Grid::dimensionworld;

    typedef typename Dune::LagrangeLocalFiniteElementCache<double, double, dim, 1> FiniteElementCache;
    typedef typename FiniteElementCache::FiniteElementType FiniteElement;

    typedef typename Dune::FieldVector<double, dimworld> GlobalCoordinate;
    typedef typename FiniteElement::Traits::LocalBasisType::Traits::JacobianType JacobianType;

    const auto& indexSet = gridView.indexSet();
    FiniteElementCache cache;

    for (const auto& element : elements(gridView))
    {
        const auto& geometry = element.geometry();
        const FiniteElement& fe = cache.get(element.type());

        int localSize = fe.size();

        // get quadrature rule of appropriate order (P1/Q1)
        int order = (element.type().isSimplex())
                    ? 2*(1-1)
                    : 2*(dim-1);
        const auto& quad = Dune::QuadratureRules<double, dim>::rule(element.type(), order);

        // store gradients of shape functions and base functions
        std::vector<JacobianType> referenceGradients(localSize);
        std::vector<GlobalCoordinate> gradients(localSize);

        for (const auto& pt : quad)
        {
            // get quadrature point
            const auto& quadPos = pt.position();

            // get transposed inverse of Jacobian of transformation
            const auto invJacobian = geometry.jacobianInverseTransposed(quadPos);

            // get integration factor
            const double integrationElement = geometry.integrationElement(quadPos);

            // evaluate gradients of shape functions
            fe.localBasis().evaluateJacobian(quadPos, referenceGradients);

            // transform gradients
            for (size_t i=0; i<gradients.size(); ++i)
                invJacobian.mv(referenceGradients[i][0], gradients[i]);

            // compute matrix entries
            double z = pt.weight() * integrationElement;
            for (int i = 0; i < localSize; ++i)
            {
                int iGlobal = indexSet.subIndex(element, fe.localCoefficients().localKey(i).subEntity(), dim);
                for (int j = i+1; j < localSize; ++j)
                {
                    int jGlobal = indexSet.subIndex(element, fe.localCoefficients().localKey(j).subEntity(), dim);

                    double zij = (gradients[i] * gradients[j]) * z;
                    Dune::MatrixVector::addToDiagonal(matrix[iGlobal][jGlobal], zij);
                    Dune::MatrixVector::addToDiagonal(matrix[jGlobal][iGlobal], zij);
                }
                double zii = (gradients[i] * gradients[i]) * z;
                Dune::MatrixVector::addToDiagonal(matrix[iGlobal][iGlobal], zii);
            }
        }
    }
}


template<class GridView, class Matrix>
void assemblePQ1Mass(const GridView& gridView, Matrix& matrix)
{
    static const int dim = GridView::Grid::dimension;

    typedef typename Dune::LagrangeLocalFiniteElementCache<double, double, dim, 1> FiniteElementCache;
    typedef typename FiniteElementCache::FiniteElementType FiniteElement;

    typedef typename FiniteElement::Traits::LocalBasisType::Traits::RangeType RangeType;

    const auto& indexSet = gridView.indexSet();
    FiniteElementCache cache;

    for (const auto& element : elements(gridView))
    {
        const auto& geometry = element.geometry();
        const FiniteElement& fe = cache.get(element.type());

        int localSize = fe.size();

        // get quadrature rule of appropriate order (P1/Q1)
        int order = (element.type().isSimplex())
                    ? 2*1
                    : 2*dim;
        const auto& quad = Dune::QuadratureRules<double, dim>::rule(element.type(), order);

        // store values of shape functions
        std::vector<RangeType> values(localSize);

        for (size_t pt=0; pt < quad.size(); ++pt)
        {
            // get quadrature point
            const auto& quadPos = quad[pt].position();

            // get integration factor
            const double integrationElement = geometry.integrationElement(quadPos);

            // evaluate basis functions
            fe.localBasis().evaluateFunction(quadPos, values);

            // compute matrix entries
            double z = quad[pt].weight() * integrationElement;
            for (int i = 0; i < localSize; ++i)
            {
                int iGlobal = indexSet.subIndex(element, fe.localCoefficients().localKey(i).subEntity(), dim);
                double zi = values[i]*z;
                for (int j = i+1; j < localSize; ++j)
                {
                    int jGlobal = indexSet.subIndex(element, fe.localCoefficients().localKey(j).subEntity(), dim);

                    double zij = values[j] * zi;
                    Dune::MatrixVector::addToDiagonal(matrix[iGlobal][jGlobal], zij);
                    Dune::MatrixVector::addToDiagonal(matrix[jGlobal][iGlobal], zij);
                }
                double zii = values[i] * zi;
                Dune::MatrixVector::addToDiagonal(matrix[iGlobal][iGlobal], zii);
            }
        }
    }
}


template<class GridView, class Vector, class Function>
void assemblePQ1RHS(const GridView& gridView, Vector& r, const Function& f)
{
    static const int dim = GridView::Grid::dimension;

    typedef typename Dune::LagrangeLocalFiniteElementCache<double, double, dim, 1> FiniteElementCache;
    typedef typename FiniteElementCache::FiniteElementType FiniteElement;

    typedef typename FiniteElement::Traits::LocalBasisType::Traits::RangeType RangeType;

    const auto& indexSet = gridView.indexSet();
    FiniteElementCache cache;

    for (const auto& element : elements(gridView))
    {
        const auto& geometry = element.geometry();
        const FiniteElement& fe = cache.get(element.type());

        int localSize = fe.size();

        // get quadrature rule of appropriate order (P1/Q1)
        int order = (element.type().isSimplex())
                    ? 2*1
                    : 2*dim;

        const auto& quad = Dune::QuadratureRules<double, dim>::rule(element.type(), order);

        // store values of shape functions
        std::vector<RangeType> values(localSize);

        for (const auto& pt : quad)
        {
            // get quadrature point
            const auto& quadPos = pt.position();

            // get integration factor
            const double integrationElement = geometry.integrationElement(quadPos);

            // evaluate basis functions
            fe.localBasis().evaluateFunction(quadPos, values);

            // evaluate function
            auto fAtPos = f(geometry.global(quadPos));

            // add vector entries
            double z = pt.weight() * integrationElement;
            for (int i = 0; i < localSize; ++i)
            {
                int iGlobal = indexSet.subIndex(element, fe.localCoefficients().localKey(i).subEntity(), dim);

                r[iGlobal].axpy(values[i]*z, fAtPos);
            }
        }
    }
}


template<class Intersection>
bool intersectionContainsVertex(const Intersection& i, int vertexInInsideElement)
{
    static const int dim = Intersection::Entity::dimension;

    int faceIdx = i.indexInInside();

    const auto& refElement = Dune::ReferenceElements<double, dim>::general(i.inside().type());

    for (int j = 0; j<refElement.size(faceIdx, 1, dim); ++j)
    {
        int intersectionVertexInElement = refElement.subEntity(faceIdx, 1, j, dim);

        if (intersectionVertexInElement == vertexInInsideElement)
            return true;
    }
    return false;
}


template<class GridView, class BitVector>
void markBoundaryDOFs(const GridView& gridView, BitVector& isBoundary)
{
    static const int dim = GridView::Grid::dimension;

    typedef typename GridView::IndexSet IndexSet;
    typedef typename Dune::LagrangeLocalFiniteElementCache<double, double, dim, 1> FiniteElementCache;
    typedef typename FiniteElementCache::FiniteElementType FiniteElement;

    const IndexSet& indexSet = gridView.indexSet();
    FiniteElementCache cache;

    for (const auto& e : elements(gridView))
    {
        const FiniteElement& fe = cache.get(e.type());
        int localSize = fe.localBasis().size();

        for (const auto& i : intersections(gridView, e))
        {
            if (i.boundary())
            {
                for (int j = 0; j < localSize; ++j)
                {
                    unsigned int vertexInInsideElement = fe.localCoefficients().localKey(j).subEntity();
                    int iGlobal = indexSet.subIndex(e, vertexInInsideElement, dim);
                    if (intersectionContainsVertex(i, vertexInInsideElement))
                        isBoundary[iGlobal] = true;
                }
            }
        }
    }
}

/**
 *
 * \tparam blocksize If this is zero, then double will be used instead of FieldVector/FieldMatrix
 */
template <size_t blocksize, class GridView, bool trivialDirichletOnly = true,
          bool makePositiveDefinitive = not trivialDirichletOnly>
class SymmetricSampleProblem {
public:
  using MatrixBlock = std::conditional_t<blocksize==0, double, Dune::FieldMatrix<double, blocksize, blocksize> >;
  using VectorBlock = std::conditional_t<blocksize==0, double, Dune::FieldVector<double, blocksize> >;
  typedef typename Dune::BCRSMatrix<MatrixBlock> Matrix;
  typedef typename Dune::BlockVector<VectorBlock> Vector;
  using BitVector = Dune::Solvers::DefaultBitVector_t<Vector>;
  typedef EnergyNorm<Matrix, Vector> Norm;

  SymmetricSampleProblem(GridView const& gridView) {
    constructPQ1Pattern(gridView, A);
    A = 0.0;
    assemblePQ1Stiffness(gridView, A);

    energyNorm.setMatrix(&A);

    ignore.resize(A.N());
    if constexpr (std::is_same_v<decltype(ignore),Dune::BitSetVector<blocksize> >)
      ignore.unsetAll();
    else
      std::fill(ignore.begin(), ignore.end(), false);
    if (trivialDirichletOnly)
        markBoundaryDOFs(gridView, ignore);
    else {
        // Mark the first component only
        BitVector boundaryNodes(A.N(), false);
        markBoundaryDOFs(gridView, boundaryNodes);
        for (size_t i=0; i < boundaryNodes.size(); ++i)
            if constexpr (std::is_same_v<decltype(ignore),Dune::BitSetVector<blocksize> >)
                ignore[i][0] = boundaryNodes[i][0];
            else
                ignore[i] = boundaryNodes[i];
    }

    u.resize(A.N());
    u_ex.resize(A.N());
    rhs.resize(A.N());
    randomize();
  }

  void randomize() {
    std::default_random_engine generator;
    std::uniform_real_distribution<double> distribution(-1, 1);

    // Set up a random 'solution'
    u = 0;
    for (size_t i = 0; i < u.size(); ++i)
      if constexpr (blocksize==0)
      {
        if (makePositiveDefinitive)
          A[i][i] += 0.5*std::abs(A[0][0]);
        u_ex[i] = distribution(generator);
      } else {
        for (size_t j = 0; j < blocksize; ++j) {
          if (makePositiveDefinitive)
            A[i][i][j][j] += 0.5*std::abs(A[0][0][0][0]);
          u_ex[i][j] = distribution(generator);
        }
      }

    // Construct right hand side corresponding to the 'solution'
    A.mv(u_ex, rhs);

    // set dirichlet values
    for (size_t i = 0; i < u.size(); ++i)
      if constexpr (blocksize==0)
      {
        if (ignore[i])
          u[i] = u_ex[i];
      } else {
        for (size_t j = 0; j < blocksize; ++j)
          if (ignore[i][j])
            u[i][j] = u_ex[i][j];
      }
  }

  Matrix A;
  Vector u;
  Vector u_ex;
  Vector rhs;
  BitVector ignore;
  Norm energyNorm;
  TwoNorm<Vector> twoNorm;
};

template<class GridType, class TestSuite>
bool checkWithGrid(TestSuite& suite, int uniformRefinements)
{
    const int dim=GridType::dimension;
    Dune::FieldVector<double,dim> lower(0);
    Dune::FieldVector<double,dim> upper(1);

    std::array<unsigned int,dim> elements;
    std::fill(elements.begin(), elements.end(), 1);

    std::shared_ptr<GridType> grid;

    if (std::is_same<GridType,Dune::YaspGrid<dim, Dune::EquidistantOffsetCoordinates<double,dim> > >::value)
      grid = Dune::StructuredGridFactory<GridType>::createCubeGrid(lower,upper,elements);
    else
      grid = Dune::StructuredGridFactory<GridType>::createSimplexGrid(lower,upper,elements);

    grid->globalRefine(uniformRefinements);

    std::cout << "Running test on " << Dune::className(*grid) << " with " << grid->size(0) << " elements." << std::endl;

    bool passed = suite.check(*grid);
    if (passed)
        std::cout << "All tests passed with " << Dune::className(*grid) << "." << std::endl;
    return passed;
}


template<class TestSuite>
bool checkWithStandardGrids(TestSuite& suite)
{
    bool passed = true;

    passed = passed and checkWithGrid<Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<double,2> > >(suite, 6);
    passed = passed and checkWithGrid<Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double,3> > >(suite, 4);

#if HAVE_DUNE_UGGRID
    passed = passed and checkWithGrid<Dune::UGGrid<2> >(suite, 5);
    passed = passed and checkWithGrid<Dune::UGGrid<3> >(suite, 3);
#endif

#if HAVE_DUNE_ALUGRID
    passed = passed and checkWithGrid<Dune::ALUGrid<2, 2, Dune::simplex, Dune::nonconforming>>(suite, 5);
    passed = passed and checkWithGrid<Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>>(suite, 3);
#endif

    return passed;
}

#endif
