// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/timer.hh>

#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/blockgssteps.hh>
#include <dune/solvers/iterationsteps/cgstep.hh>
#include <dune/solvers/iterationsteps/istlseqilu0step.hh>
#include <dune/solvers/iterationsteps/istlseqssorstep.hh>

#include "common.hh"

template <class Problem>
class Analyser {
public:
    Analyser(Problem const &p,
             double tolerance)
        : p_(p), tolerance_(tolerance)
    {}

    bool
    analyse(typename Problem::Vector const &candidate,
            SolverResult solverResult, double t, std::string testCase) {
        const auto twoNormDiff = p_.twoNorm.diff(p_.u_ex, candidate);
        printf("%f | %3zu |   %0f | %11g | %s\n", t, solverResult.iterations,
               std::pow(solverResult.conv_rate, 1.0/(1+solverResult.iterations)),
               twoNormDiff, testCase.c_str());
        if (twoNormDiff > tolerance_) {
            std::cerr << "### Error too large for solver \"" << testCase << "\"."
                      << std::endl;
            return false;
        }
        return true;
    }
private:
    Problem const &p_;
    double const tolerance_;
};

/**  \brief test for the CGStep class
  *
  *  This test tests if the Dirichlet problem for a Laplace operator
  *  is solved correctly for a random rhs by a LoopSolver employing
  *  the CGStep.
  */
template <bool withIgnore>
struct CGTestSuite
{
    template <class GridType>
    bool check(const GridType& grid)
    {
        double stepTol    = 1e-12; // termination criterion
        double solveTol   = 1e-6;  // error in the solution
        int maxIterations = 1000;

        bool passed = true;

        using Problem =
            SymmetricSampleProblem<1, typename GridType::LevelGridView, true,
                                   // no Dirichlet BC -> indefinite problem
                                   not withIgnore>;
        Problem p(grid.levelGridView(grid.maxLevel()));

        const auto verbosity = Solver::QUIET;
        const bool relativeErrors = false;

        // Test that ignoreNodes are honoured.
        if (withIgnore) {
          typename Problem::Vector u_copy = p.u;
          for (size_t i = 0; i < p.u.size(); ++i)
            if (p.ignore[i].any())
              u_copy[i] += 1.0;
          typename Problem::Vector rhs_copy = p.rhs;

          Dune::Solvers::CGStep<typename Problem::Matrix,
                                typename Problem::Vector>
              cgStep(p.A, u_copy, rhs_copy);
          cgStep.setIgnore(p.ignore);
          ::LoopSolver<typename Problem::Vector> solver(
              cgStep, maxIterations, stepTol, p.twoNorm, Solver::QUIET,
              relativeErrors, &p.u_ex);
          solver.check();
          solver.preprocess();
          solver.solve();

          for (size_t i = 0; i < p.u.size(); ++i)
            if (p.ignore[i].any())
              if (std::abs(u_copy[i] - 1.0 - p.u_ex[i]) > solveTol) {
                std::cerr << "### error: ignoredNodes not respected!"
                          << std::endl;
                passed = false;
                break;
              }
        }

        // Test if we get the right solution and benchmark
        Analyser<Problem> analyser(p, solveTol);
        Dune::Timer timer;
        printf("time [s] | #it | conv. rate | abs. error  | preconditioner\n");
        {
            std::string const testCase = "none";

            typename Problem::Vector u_copy = p.u;
            typename Problem::Vector rhs_copy = p.rhs;

            Dune::Solvers::CGStep<typename Problem::Matrix,
                                  typename Problem::Vector> cgStep(p.A, u_copy,
                                                                   rhs_copy);
            if (withIgnore)
                cgStep.setIgnore(p.ignore);
            ::LoopSolver<typename Problem::Vector> solver(
                cgStep, maxIterations, stepTol, p.twoNorm, verbosity,
                relativeErrors, &p.u_ex);
            solver.check();
            timer.reset();
            solver.preprocess();
            solver.solve();

            passed &= analyser.analyse(u_copy, solver.getResult(),
                                       timer.elapsed(), testCase);
        }
        {
            std::string const testCase = "BlockGS";

            typename Problem::Vector u_copy = p.u;
            typename Problem::Vector rhs_copy = p.rhs;

            auto blockgs =
                Dune::Solvers::BlockGSStepFactory<typename Problem::Matrix,
                                                  typename Problem::Vector>::
                    create(Dune::Solvers::BlockGS::LocalSolvers::gs(),
                           Dune::Solvers::BlockGS::Direction::SYMMETRIC);
            Dune::Solvers::CGStep<typename Problem::Matrix,
                                  typename Problem::Vector> cgStep(p.A, u_copy,
                                                                   rhs_copy,
                                                                   blockgs);
            if (withIgnore)
                cgStep.setIgnore(p.ignore);
            ::LoopSolver<typename Problem::Vector> solver(
                cgStep, maxIterations, stepTol, p.twoNorm, verbosity,
                relativeErrors, &p.u_ex);
            solver.check();
            timer.reset();
            solver.preprocess();
            solver.solve();

            passed &= analyser.analyse(u_copy, solver.getResult(), timer.elapsed(), testCase);
        }
        if (withIgnore)
            return passed;

        {
            std::string const testCase = "ISTL ilu0(1.0)";

            typename Problem::Vector u_copy = p.u;
            typename Problem::Vector rhs_copy = p.rhs;

            ISTLSeqILU0Step<typename Problem::Matrix, typename Problem::Vector>
                ilu0(1.0);
            Dune::Solvers::CGStep<typename Problem::Matrix,
                                  typename Problem::Vector>
                cgStep(p.A, u_copy, rhs_copy, ilu0);
            ::LoopSolver<typename Problem::Vector> solver(
                cgStep, maxIterations, stepTol, p.twoNorm, verbosity,
                relativeErrors, &p.u_ex);
            solver.check();
            timer.reset();
            solver.preprocess();
            solver.solve();

            passed &= analyser.analyse(u_copy, solver.getResult(),
                                       timer.elapsed(), testCase);
        }
        {
            std::string const testCase = "ISTL ssor(1, 1.0)";

            typename Problem::Vector u_copy = p.u;
            typename Problem::Vector rhs_copy = p.rhs;

            ISTLSeqSSORStep<typename Problem::Matrix, typename Problem::Vector>
                ssor(1, 1.0);
            Dune::Solvers::CGStep<typename Problem::Matrix,
                                  typename Problem::Vector>
                cgStep(p.A, u_copy, rhs_copy, ssor);
            ::LoopSolver<typename Problem::Vector> solver(
                cgStep, maxIterations, stepTol, p.twoNorm, verbosity,
                relativeErrors, &p.u_ex);
            solver.check();
            timer.reset();
            solver.preprocess();
            solver.solve();

            passed &= analyser.analyse(u_copy, solver.getResult(),
                                       timer.elapsed(), testCase);
        }
        {
            std::string const testCase = "ISTL ssor(1, 1.5)";

            typename Problem::Vector u_copy = p.u;
            typename Problem::Vector rhs_copy = p.rhs;

            ISTLSeqSSORStep<typename Problem::Matrix, typename Problem::Vector>
                ssor(1, 1.5);
            Dune::Solvers::CGStep<typename Problem::Matrix,
                                  typename Problem::Vector>
                cgStep(p.A, u_copy, rhs_copy, ssor);
            ::LoopSolver<typename Problem::Vector> solver(
                cgStep, maxIterations, stepTol, p.twoNorm, verbosity,
                relativeErrors, &p.u_ex);
            solver.check();
            timer.reset();
            solver.preprocess();
            solver.solve();

            passed &= analyser.analyse(u_copy, solver.getResult(),
                                       timer.elapsed(), testCase);
        }
        {
            std::string const testCase = "ISTL ssor(2, 1.0)";

            typename Problem::Vector u_copy = p.u;
            typename Problem::Vector rhs_copy = p.rhs;

            ISTLSeqSSORStep<typename Problem::Matrix, typename Problem::Vector>
                ssor(2, 1.0);
            Dune::Solvers::CGStep<typename Problem::Matrix,
                                  typename Problem::Vector>
                cgStep(p.A, u_copy, rhs_copy, ssor);
            ::LoopSolver<typename Problem::Vector> solver(
                cgStep, maxIterations, stepTol, p.twoNorm, verbosity,
                relativeErrors, &p.u_ex);
            solver.check();
            timer.reset();
            solver.preprocess();
            solver.solve();

            passed &= analyser.analyse(u_copy, solver.getResult(),
                                       timer.elapsed(), testCase);
        }
        {
            std::string const testCase = "ISTL ssor(2, 1.5)";

            typename Problem::Vector u_copy = p.u;
            typename Problem::Vector rhs_copy = p.rhs;

            ISTLSeqSSORStep<typename Problem::Matrix, typename Problem::Vector>
                ssor(2, 1.5);
            Dune::Solvers::CGStep<typename Problem::Matrix,
                                  typename Problem::Vector>
                cgStep(p.A, u_copy, rhs_copy, ssor);
            ::LoopSolver<typename Problem::Vector> solver(
                cgStep, maxIterations, stepTol, p.twoNorm, verbosity,
                relativeErrors, &p.u_ex);
            solver.check();
            timer.reset();
            solver.preprocess();
            solver.solve();

            passed &= analyser.analyse(u_copy, solver.getResult(),
                                       timer.elapsed(), testCase);
        }
        return passed;
    }
};

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);
    bool passed(true);

    std::cout << "Testing and benchmarking with preconditioners that can "
                 "ignore single degrees of freedom"
              << std::endl;
    CGTestSuite<true> testsuiteT;
    passed &= checkWithStandardGrids(testsuiteT);

    std::cout << std::endl;

    std::cout << "Testing and benchmarking with preconditioners that cannot "
                 "ignore single degrees of freedom"
              << std::endl;
    CGTestSuite<false> testsuiteF;
    passed &= checkWithStandardGrids(testsuiteF);

    return passed ? 0 : 1;
}
