// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <cmath>
#include <iostream>
#include <sstream>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/istl/bcrsmatrix.hh>

#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/solvers/cholmodsolver.hh>


#include "common.hh"

using namespace Dune;

/**  \brief test for the CholmodSolver class
  *
  *  This test tests whether the Dirichlet problem for a Laplace operator is solved correctly for a "random" rhs by an CholmodSolver.
  *  Implementation is more or less stolen from UMFPackSolverTest
  */
template <size_t blocksize>
struct CHOLMODSolverTestSuite
{
  template <class GridType>
  bool check(const GridType& grid)
  {
    double tol = 1e-12;

    bool passed = true;

    using Problem =
        SymmetricSampleProblem<blocksize, typename GridType::LeafGridView>;
    Problem p(grid.leafGridView());

    typedef Solvers::CholmodSolver<typename Problem::Matrix,
                                   typename Problem::Vector> Solver;
    Solver solver(p.A,p.u,p.rhs);
    solver.setIgnore(p.ignore);

    // solve problem
    solver.preprocess();
    solver.solve();

    if (p.energyNorm.diff(p.u,p.u_ex)>tol*10)
    {
      std::cout << "The CholmodSolver did not produce a satisfactory result. ||u-u_ex||=" << p.energyNorm.diff(p.u,p.u_ex) << std::endl;
      std::cout << "||u_ex||=" << p.energyNorm(p.u_ex) << std::endl;
      passed = false;
    }

    return passed;
  }
};

// Check whether we can reuse a factorization for several right hand sides
bool checkMultipleSolves()
{
  bool passed = true;

  using Matrix = BCRSMatrix<double>;
  using Vector = BlockVector<double>;

  // Construct an example matrix
  Matrix matrix(4, 4,
                3,    // Expected average number of nonzeros
                      // per row
                0.2,  // Size of the compression buffer,
                      // as a fraction of the expected total number of nonzeros
                BCRSMatrix<double>::implicit);

  matrix.entry(0,0) = 2;
  matrix.entry(0,1) = 1;
  matrix.entry(1,0) = 1;
  matrix.entry(1,1) = 2;
  matrix.entry(1,2) = 1;
  matrix.entry(2,1) = 1;
  matrix.entry(2,2) = 2;
  matrix.entry(2,3) = 1;
  matrix.entry(3,2) = 1;
  matrix.entry(3,3) = 2;

  matrix.compress();

  // Set up the solver
  Solvers::CholmodSolver<Matrix,Vector> cholmodSolver;

  // Where to write the solution
  Vector x(4);
  cholmodSolver.setSolutionVector(x);

  // Factorize the matrix
  cholmodSolver.setMatrix(matrix);
  cholmodSolver.factorize();

  // Test with a particular right-hand side
  Vector rhs = {3,4,4,3};

  // The corresponding solution
  Vector reference = {1,1,1,1};

  cholmodSolver.setRhs(rhs);
  cholmodSolver.solve();

  // Check whether result is correct
  auto diff = reference;
  diff -= x;
  if (diff.infinity_norm() > 1e-8)
  {
    std::cerr << "CholmodSolver didn't produce the correct result!" << std::endl;
    passed = false;
  }

  // Test with a different right-hand side
  // This reuses the original factorization.
  rhs = {4,6,6,5};
  reference = {1,2,1,2};
  cholmodSolver.solve();

  diff = reference;
  diff -= x;
  if (diff.infinity_norm() > 1e-8)
  {
    std::cerr << "CholmodSolver didn't produce the correct result!" << std::endl;
    passed = false;
  }

  return passed;
}

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    bool passed = true;

    CHOLMODSolverTestSuite<1> testsuite1;
    CHOLMODSolverTestSuite<2> testsuite2;
    passed &= checkWithStandardGrids(testsuite1);
    passed &= checkWithStandardGrids(testsuite2);

    passed &= checkMultipleSolves();

    return passed ? 0 : 1;
}
