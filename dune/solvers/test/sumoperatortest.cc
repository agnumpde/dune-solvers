// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <cmath>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/diagonalmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/matrix.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/scaledidmatrix.hh>
#include <dune/istl/io.hh>

#include <dune/solvers/operators/sumoperator.hh>
#include <dune/solvers/operators/lowrankoperator.hh>

#define BLOCKSIZE 2

// This tests the SumOperator's methods for consistency with successive execution of corresponding operations
template <class SparseMatrixType, class LowRankFactorType>
bool check()
{

    bool passed = true;
    typedef LowRankOperator<LowRankFactorType> LowRankMatrixType;
    typedef typename SparseMatrixType::block_type  sp_block_type;
    typedef typename LowRankFactorType::block_type lr_block_type;
    size_t block_size = sp_block_type::rows;

    assert(block_size==lr_block_type::rows);

    size_t m=3, n=10;
    typedef Dune::FieldVector<typename sp_block_type::field_type, sp_block_type::rows> Vblock_type;
    typedef Dune::BlockVector<Vblock_type> Vector;

    Vector x(n), b(n), c(n), ref_vec(n);
    for (size_t i = 0; i<n; ++i)
//        x[i] = 1.0;
        x[i] = (1.0*rand()) / RAND_MAX;
    b = c = ref_vec = 0.0;

    SumOperator<SparseMatrixType,LowRankMatrixType> sum_op_default;

    LowRankFactorType lr_factor(m,n);
    SparseMatrixType sparse_matrix(n,n,3*n,SparseMatrixType::row_wise);


    // create sparse Matrix as tridiagonal matrix
    typedef typename SparseMatrixType::CreateIterator Iter;
    Iter c_end = sparse_matrix.createend();
    for(Iter row=sparse_matrix.createbegin(); row!=c_end; ++row)
    {
        // Add nonzeros for left neighbour, diagonal and right neighbour
        if(row.index()>0)
            row.insert(row.index()-1);
        row.insert(row.index());
        if(row.index()<sparse_matrix.N()-1)
            row.insert(row.index()+1);
    }

     // Now the sparsity pattern is fully set up and we can add values
    double entry=0.0;
    for (size_t i=0; i<n; ++i)
    {
        if (i>0)
        {
            entry = (1.0*rand())/RAND_MAX;
            sparse_matrix[i][i-1] = entry;
        }
        entry = (1.0*rand())/RAND_MAX;
        sparse_matrix[i][i]= entry;
        if (i<n-1)
        {
            entry = (1.0*rand())/RAND_MAX;
            sparse_matrix[i][i+1] = entry;
        }
    }

    sum_op_default.sparseMatrix() = sparse_matrix;

    // set lowrankfactor to random values
    typedef typename lr_block_type::RowIterator BlockRowIterator;
    typedef typename lr_block_type::ColIterator BlockColIterator;
    for (size_t row=0; row<m; ++row)
        for (size_t col=0; col<n; ++col)
        {
            BlockRowIterator row_end = lr_factor[row][col].end();
            for (BlockRowIterator row_it = lr_factor[row][col].begin(); row_it!=row_end; ++ row_it)
            {
                BlockColIterator col_end = row_it->end();
                for (BlockColIterator col_it = row_it->begin(); col_it!=col_end; ++col_it)
//                    *col_it = col+1;
                    *col_it = (1.0*rand())/RAND_MAX;
            }
        }

    sum_op_default.lowRankMatrix().lowRankFactor() = lr_factor;

    LowRankMatrixType lr_op(lr_factor);

    std::cout << "Checking SumOperator<" << Dune::className(sparse_matrix) << ", " << Dune::className(lr_op) << " >  ...  ";

    SumOperator<SparseMatrixType,LowRankOperator<LowRankFactorType> > sum_op(sparse_matrix, lr_op);


    lr_op.umv(x,ref_vec);
    sparse_matrix.umv(x,ref_vec);

    sum_op.umv(x,b);
    sum_op_default.umv(x,c);
    for (size_t i=0; i<b.size(); ++i)
    if ((b[i] - ref_vec[i]).two_norm()>1e-12 or (c[i] - ref_vec[i]).two_norm()>1e-12)
    {
        std::cout << "SumOperator::umv does not yield correct result." << std::endl;
        passed = false;
        for (size_t j=0; j<b.size(); ++j)
            std::cout << b[j] << "     "<<ref_vec[j] << std::endl;
        std::cout << std::endl;
        break;
    }

    c=b=1.0;

    sum_op.mv(x,b);
    sum_op_default.mv(x,c);
    for (size_t i=0; i<b.size(); ++i)
    if ((b[i] - ref_vec[i]).two_norm()>1e-12 or (c[i] - ref_vec[i]).two_norm()>1e-12)
    {
        std::cout << "SumOperator::mv does not yield correct result." << std::endl;
        passed = false;
        break;
    }

    sum_op.mmv(x,b);
    sum_op_default.mmv(x,c);
    for (size_t i=0; i<b.size(); ++i)
    if (b[i].two_norm()>1e-12 or c[i].two_norm()>1e-12)
    {
        std::cout << "SumOperator::mmv does not yield correct result." << std::endl;
        passed = false;
        break;
    }


    LowRankOperator<LowRankFactorType> lr_factor_reborn = sum_op.lowRankMatrix();
    SparseMatrixType sparse_matrix_reborn = sum_op.sparseMatrix();


    /* test for consistency of return value of N() */
    if (sum_op.N() != sparse_matrix.N())
    {
        std::cout << "SumOperator::N does not return correct value for the SumOperator constructed from given matrices. (returns " << sum_op.N() << ", should return " << sum_op.sparseMatrix().N() << ")" << std::endl;
        passed = false;
    }
    if (sum_op_default.N() !=sparse_matrix.N())
    {
        std::cout << "SumOperator::N does not return correct value for the default-constructed SumOperator." << std::endl;
        passed = false;
    }

    if (passed)
        std::cout << "passed";
    std::cout << std::endl;
    return passed;
}

bool checkResize() try {
  std::cout << "Checking resize from SumOperator ... ";
  using namespace Dune;
  using Block = FieldMatrix<double, 2, 6>;
  using SparseMatrix = BCRSMatrix<Block>;
  SparseMatrix sparseMatrix;
  MatrixIndexSet indices(3,4);
  indices.exportIdx(sparseMatrix);
  using LowRankFactor = Matrix<Block>;
  SumOperator<SparseMatrix, LowRankOperator<LowRankFactor>> sumOp;
  sumOp.sparseMatrix() = sparseMatrix;
  sumOp.lowRankMatrix().lowRankFactor() = LowRankFactor(5,6); // "weird" sizes should not matter
  BlockVector<Dune::FieldVector<double, 2>> vector;
  MatrixVector::resize(vector, sumOp);
  if(vector.size() != 3)
    DUNE_THROW(Exception, "Resize yields unexpected vector size.");
  std::cout << "passed" << std::endl;
  return true;
} catch(const Dune::Exception& e) {
  std::cout << " FAILED." << std::endl;
  std::cout << "Exception was: " << e << std::endl;
  return false;
}

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    static const int block_size = BLOCKSIZE;

    bool passed(true);

    typedef Dune::ScaledIdentityMatrix<double, block_size>    Block1;
    typedef Dune::DiagonalMatrix<double, block_size>          Block2;
    typedef Dune::FieldMatrix<double, block_size, block_size> Block3;

    passed =            check<Dune::BCRSMatrix<Block1>, Dune::Matrix<Block1> >();
    passed = passed and check<Dune::BCRSMatrix<Block1>, Dune::Matrix<Block2> >();
    passed = passed and check<Dune::BCRSMatrix<Block1>, Dune::Matrix<Block3> >();

    passed = passed and check<Dune::BCRSMatrix<Block2>, Dune::Matrix<Block1> >();
    passed = passed and check<Dune::BCRSMatrix<Block2>, Dune::Matrix<Block2> >();
    passed = passed and check<Dune::BCRSMatrix<Block2>, Dune::Matrix<Block3> >();

    passed = passed and check<Dune::BCRSMatrix<Block3>, Dune::Matrix<Block1> >();
    passed = passed and check<Dune::BCRSMatrix<Block3>, Dune::Matrix<Block2> >();
    passed = passed and check<Dune::BCRSMatrix<Block3>, Dune::Matrix<Block3> >();

    passed = passed and checkResize();

    return passed ? 0 : 1;
}
