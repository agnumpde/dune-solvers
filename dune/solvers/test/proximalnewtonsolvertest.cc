// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <cmath>
#include <iostream>
#include <sstream>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/solvers/norms/energynorm.hh>
#include <dune/solvers/norms/twonorm.hh>
#include <dune/solvers/solvers/cholmodsolver.hh>
#include <dune/solvers/solvers/proximalnewtonsolver.hh>

#include "common.hh"

using namespace Dune;

// grid dimension
const int dim = 3;

// f(x) = 1/4 * ||x||^4
template<class Matrix, class Vector>
struct F
{
  void assembleGradientAndHessian( const Vector& x, Vector& grad, Matrix& hess ) const
  {
    auto norm2 = x.two_norm2();

    grad = x;
    grad *= norm2;

    for(size_t i=0; i<x.size(); i++)
      for(size_t j=0; j<x.size(); j++)
        hess[i][j] = (i==j)*norm2 + 2*x[i]*x[j];
  }

  double computeEnergy( const Vector& x ) const
  {
    return  0.25* x.two_norm2() * x.two_norm2();
  }
};


// g(x) = ( -1, -1, -1, ... , -1)^T * x
template<class Vector>
struct G
{
  double operator()( const Vector& x ) const
  {
    auto realX = x;
    realX += offset_;

    double sum = 0.0;
    for ( size_t i=0; i<realX.size(); i++ )
      sum -= realX[i];

    return sum;
  }

  void updateOffset( const Vector& offset )
  {
    offset_ = offset;
  }

  Vector offset_;
};

template<class Matrix, class Vector>
struct SecondOrdersolver
{
  using MatrixType = Matrix;

  template<class BitVector>
  void minimize( const Matrix& hess, const Vector& grad, const G<Vector>& g, double reg, Vector& dx, const BitVector& /*ignore*/) const
  {
    // add reg
    auto HH = hess;
    for ( size_t i=0; i<dx.size(); i++ )
      HH[i][i] += reg;

    // add g to grad
    auto gg = grad;
    gg *= -1.0;
    for ( size_t i=0; i<dx.size(); i++ )
      gg[i] += 1.0;


    Solvers::CholmodSolver cholmod( HH, dx, gg );
    cholmod.solve();
  }
};



int main (int argc, char *argv[])
{
  // initialize MPI, finalize is done automatically on exit
  [[maybe_unused]] MPIHelper& mpiHelper = MPIHelper::instance(argc, argv);

  const int size = 10;

  using Vector = FieldVector<double,size>;
  using Matrix = FieldMatrix<double,size>;
  using BitVector = std::bitset<size>;

  // this is the analytical solution of the problem
  Vector exactSol, zeroVector;
  exactSol = std::pow( size, -1.0/3.0 );

  zeroVector = 0.0;

  F<Matrix,Vector> f;
  G<Vector> g;
  g.updateOffset(zeroVector);
  SecondOrdersolver<Matrix,Vector> sos;


  // choose some random initial vector
  Vector sol;
  for ( size_t i=0; i<sol.size(); i++ )
    sol[i] = i;

  TwoNorm<Vector> norm;

  // create the solver
  Solvers::ProximalNewton::SimpleRegUpdater regUpdater;
  double reg = 42.0;
  Solvers::ProximalNewtonSolver proximalNewtonSolver( f, g, sos, sol, norm, regUpdater, reg, 100, 1e-14, Solver::FULL);

  // set some empty ignore field
  BitVector ignore;
  proximalNewtonSolver.setIgnore( ignore );

  // go!
  proximalNewtonSolver.solve();

  // compute diff the exact solution
  sol -= exactSol;

  std::cout << "Difference to exact solution = " << sol.two_norm() << std::endl;

  bool passed = sol.two_norm() < 1e-15;

  return passed ? 0 : 1;
}
