// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_COMPUTEENERGY_HH
#define DUNE_SOLVERS_COMPUTEENERGY_HH

#include <dune/matrix-vector/axy.hh>

template <class M, class V>
double computeEnergy(const M& mat, const V& x)
{
    return 0.5*Dune::MatrixVector::Axy(mat,x,x);
}

template <class M, class V>
double computeEnergy(const M& mat, const V& x, const V& rhs)
{
    return computeEnergy(mat,x) - rhs*x;
}
#endif
