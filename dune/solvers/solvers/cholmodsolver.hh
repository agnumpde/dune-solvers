// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_SOLVERS_CHOLMODSOLVER_HH
#define DUNE_SOLVERS_SOLVERS_CHOLMODSOLVER_HH

/** \file
    \brief A wrapper for the CHOLMOD sparse direct solver
*/

#include <dune/common/exceptions.hh>
#include <dune/common/bitsetvector.hh>
#include <dune/common/fmatrix.hh>

#include <dune/istl/solver.hh>
#include <dune/istl/cholmod.hh>
#include <dune/istl/foreach.hh>
#include <dune/istl/matrixindexset.hh>
#include <dune/istl/io.hh>

#include <dune/solvers/common/canignore.hh>
#include <dune/solvers/solvers/linearsolver.hh>
#include <dune/solvers/common/defaultbitvector.hh>

namespace Dune
{

namespace Solvers
{

/** \brief Wraps the CHOLMOD sparse symmetric direct solver
*
*  CHOLMOD uses the Cholesky decomposition A = LL^T for sparse
*  symmetric matrices to solve linear systems Ax = b efficiently.
*
*  This class wraps the dune-istl CHOLMOD solver for dune-solvers to provide
*  a Solvers::LinearSolver interface.
*/
template <class MatrixType, class VectorType, class BitVectorType = DefaultBitVector_t<VectorType>>
class CholmodSolver
: public LinearSolver<MatrixType,VectorType>, public CanIgnore<BitVectorType>
{
  using field_type = typename VectorType::field_type;

public:

  /** \brief Default constructor */
  CholmodSolver ()
  : LinearSolver<MatrixType,VectorType>(NumProc::FULL)
  {
    // Bug in LibScotchMetis:
    // metis_dswitch: see cholmod.h: LibScotchMetis throws segmentation faults for large problems.
    // This caused a hard to understand CI problems and therefore, METIS is disabled for problems
    // larger than defined in metis_nswitch for now (default 3000).
    // Bug report: https://gitlab.inria.fr/scotch/scotch/issues/8
    cholmodCommonObject().metis_dswitch = 0.0;
  }

  /** \brief Constructor for a linear problem */
  CholmodSolver (const MatrixType& matrix,
                 VectorType& x,
                 const VectorType& rhs,
                 NumProc::VerbosityMode verbosity=NumProc::FULL)
  : LinearSolver<MatrixType,VectorType>(verbosity),
    matrix_(&matrix),
    x_(&x),
    rhs_(&rhs)
  {
    // Bug in LibScotchMetis:
    // metis_dswitch: see cholmod.h: LibScotchMetis throws segmentation faults for large problems.
    // This caused a hard to understand CI problems and therefore, METIS is disabled for problems
    // larger than defined in metis_nswitch for now (default 3000).
    // Bug report: https://gitlab.inria.fr/scotch/scotch/issues/8
    cholmodCommonObject().metis_dswitch = 0.0;
  }

  /** \brief Set the linear problem to solve
   *
   * The matrix is assumed to be symmetric! CHOLMOD uses only the
   * upper part of the matrix, the lower part is ignored and can be
   * empty for optimal memory use.
   */
  void setProblem(const MatrixType& matrix,
                  VectorType& x,
                  const VectorType& rhs) override
  {
    setMatrix(matrix);
    setSolutionVector(x);
    setRhs(rhs);
  }

  /** \brief Set the matrix for the linear linear problem to solve
   *
   * The matrix is assumed to be symmetric! CHOLMOD uses only the
   * upper part of the matrix, the lower part is ignored and can be
   * empty for optimal memory usage.
   *
   * \warning Unlike the setMatrix method of the dune-istl Cholmod class,
   * the method here does not factorize the matrix!   Call the factorize
   * method for that.
   */
  void setMatrix(const MatrixType& matrix)
  {
    matrix_ = &matrix;
    isFactorized_ = false;
  }

  /** \brief Set the rhs vector for the linear problem
   */
  void setRhs(const VectorType& rhs)
  {
    rhs_ = &rhs;
  }

  /** \brief Set the vector where to write the solution of the linear problem
   */
  void setSolutionVector(VectorType& x)
  {
    x_ = &x;
  }

  /** \brief Compute the Cholesky decomposition of the matrix
   *
   * You must have set a matrix to be able to call this,
   * either by calling setProblem or the appropriate constructor.
   *
   * The degrees of freedom to ignore must have been set before
   * calling the `factorize` method.
   */
  void factorize()
  {
    if (not this->hasIgnore())
    {
      // setMatrix will decompose the matrix
      istlCholmodSolver_.setMatrix(*matrix_);

      // get the error code from Cholmod in case something happened
      errorCode_ = cholmodCommonObject().status;
    }
    else
    {
      const auto& ignore = this->ignore();

      // The setMatrix method stores only the selected entries of the matrix (determined by the ignore field)
      istlCholmodSolver_.setMatrix(*matrix_,&ignore);
      // get the error code from Cholmod in case something happened
      errorCode_ = cholmodCommonObject().status;
    }

    if (errorCode_ >= 0)  // okay or warning, but no error
      isFactorized_ = true;
  }

  /** \brief Solve the linear system
   *
   * This factorizes the matrix if it hasn't been factorized earlier
   */
  virtual void solve() override
  {
    // prepare the solver
    Dune::InverseOperatorResult statistics;

    // Factorize the matrix now, if the caller hasn't done so explicitly earlier.
    if (!isFactorized_)
      factorize();

    if (not this->hasIgnore())
    {
      // the apply() method doesn't like constant values
      auto mutableRhs = *rhs_;

      // The back-substitution
      istlCholmodSolver_.apply(*x_, mutableRhs, statistics);
    }
    else
    {
      const auto& ignore = this->ignore();

      // create flat vectors
      using T = typename VectorType::field_type;
      std::size_t flatSize = flatVectorForEach(ignore, [](...){});
      std::vector<bool> flatIgnore(flatSize);
      std::vector<T> flatX(flatSize), flatRhsModifier(flatSize);

      // define a lambda that copies the entries of a blocked vector into a flat one
      auto copyToFlat = [&](auto&& blocked, auto&& flat)
      {
        flatVectorForEach(blocked, [&](auto&& entry, auto&& offset)
        {
          flat[offset] = entry;
        });
      };

      // copy x and the ignore field for the modification of the rhs
      copyToFlat(ignore,flatIgnore);
      copyToFlat(*x_,flatX);

      flatMatrixForEach( *matrix_, [&]( auto&& entry, auto&& rowOffset, auto&& colOffset){

        // we assume entries only in the upper part and do nothing on the diagonal
        if ( rowOffset >= colOffset )
          return;

        // upper part: if either col or row is ignored: move the entry or the symmetric duplicate to the rhs
        if ( flatIgnore[colOffset] and not flatIgnore[rowOffset] )
            flatRhsModifier[rowOffset] += entry * flatX[colOffset];

        else if ( not flatIgnore[colOffset] and flatIgnore[rowOffset] )
            flatRhsModifier[colOffset] += entry * flatX[rowOffset];
      });

      // update the rhs
      auto modifiedRhs = *rhs_;
      flatVectorForEach(modifiedRhs, [&](auto&& entry, auto&& offset){
        if ( not flatIgnore[offset] )
          entry -= flatRhsModifier[offset];
      });

      // Solve the modified system
      istlCholmodSolver_.apply(*x_, modifiedRhs, statistics);
    }
  }

  cholmod_common& cholmodCommonObject()
  {
    return istlCholmodSolver_.cholmodCommonObject();
  }

  /** \brief return the error code of the Cholmod factorize call
   *
   * In setMatrix() the matrix factorization takes place and Cholmod is
   * able to communicate error codes of the factorization in the status
   * field of the cholmodCommonObject.
   * The return value 0 means "good" and the other values can be found
   * in the Cholmod User Guide.
   */
  int errorCode() const
  {
    return errorCode_;
  }

  //! dune-istl Cholmod solver object
  Dune::Cholmod<VectorType> istlCholmodSolver_;

  //! Pointer to the system matrix
  const MatrixType* matrix_;

  //! Pointer to the solution vector
  VectorType* x_;

  //! Pointer to the right hand side
  const VectorType* rhs_;

  //! error code of Cholmod factorize call
  int errorCode_ = 0;

  /** \brief Whether istlCholmodSolver_ currently holds a factorized matrix.
   *
   * isFactorized==true is equivalent to istlCholmodSolver_->L_ != nullptr,
   * but the latter information is private to the dune-istl Cholmod class.
   */
  bool isFactorized_ = false;
};

}   // namespace Solvers
}   // namespace Dune

#endif
