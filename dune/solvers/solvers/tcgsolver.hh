// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_SOLVERS_TCGSOLVER_HH
#define DUNE_SOLVERS_SOLVERS_TCGSOLVER_HH

#include <cmath>

#include <dune/matrix-vector/axy.hh>

#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/iterationsteps/lineariterationstep.hh>
#include <dune/solvers/norms/norm.hh>

/** \brief A truncated conjugate gradient solver
 *
 */
template <class MatrixType, class VectorType>
class TruncatedCGSolver : public IterativeSolver<VectorType>
{
    static const int blocksize = VectorType::block_type::dimension;

    typedef typename VectorType::field_type field_type;

    /** \brief Returns positive root of a quadratic equation
    */
    static field_type positiveRoot(const field_type& a, const field_type& b, const field_type& c) {

        field_type p = b/a;
        field_type q = c/a;

        field_type root1 = - p/2 - Dune::sign(p) * std::sqrt(p*p/4 - q);
        field_type root2 = q/root1;

        // There must be one positive and one negative root
        assert(root1*root2 <= 0);

        return std::max(root1, root2);
    }

    /** \brief Computes <Mb,a>, where M is the norm that defines the trust region */
    field_type trustRegionScalarProduct(const VectorType& a,
                                        const VectorType& b) const {

        if (trustRegionNormMatrix_)
            return Dune::MatrixVector::Axy(*trustRegionNormMatrix_, b, a);

        return a*b;

    }

public:

    /** \brief Constructor taking all relevant data */
    TruncatedCGSolver(const MatrixType* matrix,
                      VectorType* x,
                      VectorType* rhs,
                      LinearIterationStep<MatrixType,VectorType>* preconditioner,
                      int maxIterations,
                      double tolerance,
                      Norm<VectorType>* errorNorm,
                      const MatrixType* trustRegionNormMatrix,
                      double trustRegionRadius,
                      NumProc::VerbosityMode verbosity,
                      bool useRelativeError=true)
        : IterativeSolver<VectorType>(tolerance,maxIterations,verbosity,useRelativeError),
          matrix_(matrix), x_(x), rhs_(rhs),
          preconditioner_(preconditioner),
          errorNorm_(errorNorm),
          trustRegionRadius_(trustRegionRadius),
          trustRegionNormMatrix_(trustRegionNormMatrix)
    {}

    /** \brief Constructor for repeated calls.  The actual problem has to be provided separately */
    TruncatedCGSolver(LinearIterationStep<MatrixType,VectorType>* preconditioner,
                      int maxIterations,
                      double tolerance,
                      Norm<VectorType>* errorNorm,
                      const MatrixType* trustRegionNormMatrix,
                      NumProc::VerbosityMode verbosity,
                      bool useRelativeError=true)
        : IterativeSolver<VectorType>(tolerance,maxIterations,verbosity,useRelativeError),
          matrix_(NULL), x_(NULL), rhs_(NULL),
          preconditioner_(preconditioner),
          errorNorm_(errorNorm),
          trustRegionRadius_(0),
          trustRegionNormMatrix_(trustRegionNormMatrix)
    {}

    void setProblem(const MatrixType& matrix,
               VectorType* x,
               VectorType* rhs,
               field_type trustRegionRadius)
    {
        matrix_ = &matrix;
        x_      = x;
        rhs_    = rhs;
        trustRegionRadius_ = trustRegionRadius;
    }

    /** \brief Loop, call the iteration procedure
     * and monitor convergence */
    virtual void solve();

    /** \brief Checks whether all relevant member variables are set
     * \exception SolverError if the iteration step is not set up properly
     */
    virtual void check() const;

    const MatrixType* matrix_;

    VectorType* x_;

    VectorType* rhs_;

    //! The iteration step used by the algorithm
    LinearIterationStep<MatrixType,VectorType>* preconditioner_;

    //! The norm used to measure convergence
    Norm<VectorType>* errorNorm_;

    /** \brief Radius of the admissible set */
    field_type trustRegionRadius_;

    /** \brief Norm that the admissible set is measured in */
    const MatrixType* trustRegionNormMatrix_;
};

#include "tcgsolver.cc"

#endif
