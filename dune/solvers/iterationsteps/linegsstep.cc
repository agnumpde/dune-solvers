// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#include <dune/istl/btdmatrix.hh>
#include <dune/istl/scaledidmatrix.hh>



template<class MatrixType, class DiscFuncType, class BitVectorType>
void LineGSStep<MatrixType, DiscFuncType, BitVectorType >::iterate()
{

    // input of this method: x^(k) (not the permuted version of x^(k)!)

    const MatrixType& mat = *this->mat_;

    int number_of_blocks = blockStructure_.size();

    // we make a 'permuted' iteration step, which means that we use the permuted matrix and the permuted right hand side, instead of the original one:

    // iterate over the blocks
    for (int b_num=0; b_num < number_of_blocks; b_num++ ) {


        const int current_block_size = blockStructure_[b_num].size();

        //! compute and save the residuals for the current block:
        // determine the (permuted) residuals r[p(i)],..., r[p(i+current_block_size-1)]
        // this means that we determine the residuals for the current block
        DiscFuncType permuted_r_i(current_block_size);
        for (int k=0; k<current_block_size; k++)
          {
           if ( this->ignore()[blockStructure_[b_num][k]][0])
               permuted_r_i[k] = 0.0;
           else
               this->residual( blockStructure_[b_num][k], permuted_r_i[k]); // get r[p(i+k)]
          }
        // permuted_r_i[k] is the residual for r[p(i+k)], which means the residual \tilde{r}[i+k]
        // note: calling the method 'residual' with the permuted index implies that there is no additional permutation required within 'residual'

        // permuted_v_i is for saving the correction for x[p(i)],..., x[p(i+current_block_size-1)]:
        DiscFuncType permuted_v_i(current_block_size); // permuted_v_i[k] = v[p(i+k)]
        // (Later: x_now[p(i+k)] = x_now[p(i+k)] + v[p(i+k)] )

        // //////////////////////////////////////////////////////////////////////////////////////////////////
        // Copy the linear system for the current line/block into a tridiagonal matrix
        // //////////////////////////////////////////////////////////////////////////////////////////////////

        Dune::BTDMatrix<typename MatrixType::block_type> tridiagonalMatrix(current_block_size);

        for (int j=0; j<current_block_size; j++) {

            if ( this->ignore()[blockStructure_[b_num][j]][0] ) {

                // left off-diagonal:
                if (j>0)
                    tridiagonalMatrix[j][j-1] = 0;

                // diagonal
                tridiagonalMatrix[j][j] = Dune::ScaledIdentityMatrix<double,BlockSize>(1);

                // right off-diagonal:
                if (j<current_block_size-1)
                    tridiagonalMatrix[j][j+1] = 0;

            } else {

                // left off-diagonal:
                if (j>0)
                    tridiagonalMatrix[j][j-1] = mat[blockStructure_[b_num][j]][blockStructure_[b_num][j-1]];

                // diagonal
                tridiagonalMatrix[j][j] = mat[blockStructure_[b_num][j]][blockStructure_[b_num][j]];

                // right off-diagonal:
                if (j<current_block_size-1)
                    tridiagonalMatrix[j][j+1] = mat[blockStructure_[b_num][j]][blockStructure_[b_num][j+1]];

            }

        }

        // ////////////////////////////////////////
        //   Solve the tridiagonal system
        // ////////////////////////////////////////
        tridiagonalMatrix.solve(permuted_v_i, permuted_r_i);

        // //////////////////////////////////////////////////////////////////////
        //   Add the correction to the current iterate
        // //////////////////////////////////////////////////////////////////////
       for (int k=0; k<current_block_size; k++)
           (*this->x_)[blockStructure_[b_num][k]] += permuted_v_i[k];

    }

}
