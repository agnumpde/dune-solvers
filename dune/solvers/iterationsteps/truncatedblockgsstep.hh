// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef TRUNCATED_BLOCK_GAUSS_SEIDEL_STEP_HH
#define TRUNCATED_BLOCK_GAUSS_SEIDEL_STEP_HH

#include <cmath>

#include <dune/common/bitsetvector.hh>

#include <dune/matrix-vector/axpy.hh>
#include <dune/matrix-vector/promote.hh>

#include <dune/solvers/iterationsteps/lineariterationstep.hh>
#include <dune/solvers/operators/sumoperator.hh>

namespace TruncatedBlockGSStepNS {
template<int blocklevel> struct RecursiveGSStep;
}



template<class MatrixType, class VectorType, class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension> >
class TruncatedBlockGSStep
    : public LinearIterationStep<MatrixType, VectorType, BitVectorType>
{
    typedef typename VectorType::block_type VectorBlock;
    typedef typename VectorType::field_type field_type;

public:

    //! Default constructor.  Doesn't init anything
    TruncatedBlockGSStep(int innerLoops=1) :
        innerLoops_(innerLoops)
    {}

    //! Constructor with a linear problem
    TruncatedBlockGSStep(const MatrixType& mat, VectorType& x, const VectorType& rhs, int innerLoops=1)
        : LinearIterationStep<MatrixType,VectorType>(mat, x, rhs),
        innerLoops_(innerLoops)
    {}

    //! Perform one iteration
    virtual void iterate()
    {
        TruncatedBlockGSStepNS::RecursiveGSStep<Dune::blockLevel<MatrixType>()>::apply(*this->mat_, *this->rhs_, this->ignore(), *this->x_, innerLoops_);
    }

protected:
    int innerLoops_;
};


// additional namespace since template specialization inside
// of other template is not allowed
namespace TruncatedBlockGSStepNS {

//! Recursive Gauß-Seidel step
// additional struct is needed since partial specialization of
// function templates is not allowed
template<int blocklevel>
struct RecursiveGSStep
{
    template<class MType, class VType, class BVType>
    static void apply(const MType& mat, const VType& rhs, const BVType& ignore, VType& x, int innerLoops)
    {
        typedef typename MType::row_type::ConstIterator ColumnIterator;
        typedef typename MType::block_type MBlock;
        typedef typename VType::block_type VBlock;

        for (size_t row=0; row<mat.N(); ++row)
        {
            VBlock r = rhs[row];
            const MBlock* Aii=0;

            ColumnIterator it = mat[row].begin();
            ColumnIterator end = mat[row].end();
            for(; it!=end; ++it)
            {
                size_t col = it.index();
                if (col == row)
                    Aii = &(*it);
                else
                    it->mmv(x[col],r);
            }

            if (Aii!=0)
                for(int i=0; i<innerLoops; ++i)
                    RecursiveGSStep<MBlock::blocklevel>::apply(*Aii, r, ignore[row], x[row], innerLoops);
        }
    }
};

//! Recursion end for blockevel 1
// use optimal implementations provided by StaticMatrix
template<>
struct RecursiveGSStep<1>
{
    template<class MType, class VType, class BVType>
    static void apply(const MType& mat, const VType& rhs, const BVType& ignore, VType& x, [[maybe_unused]] int innerLoops)
    {
        typedef typename MType::block_type MBlock;

        for (size_t i=0; i<mat.N(); ++i)
        {
            const MBlock& Aii = mat[i][i];
            if (Aii>0)
            {
                if (not(ignore[i]))
                {
                    x[i] = rhs[i];
                    typename MType::row_type::ConstIterator it = mat[i].begin();
                    typename MType::row_type::ConstIterator end = mat[i].end();
                    for(; it!=end; ++it)
                        if (it.index()!=i)
                            x[i] -= (*it) * x[it.index()];
                    x[i] /= Aii;
                }
            }
        }
    }
};

} // end namespace TruncatedBlockGSStepNS

/**  \brief nonrecursive specialization of the TruncatedBlockGSStep
  *
  *  \tparam SparseMatrixType the type of the sparse matrix in the SumOperator
  *  \tparam LowRankMatrixType the type of the lowrank matrix in the SumOperator
  *  \tparam VectorType the type of the solution vector
  *  \tparam BitVectorType the type of the truncation field
  *  \tparam localSolver static switch for local solver, see specializations of method localSolve
  */
template<class SparseMatrixType, class LowRankMatrixType, class VectorType, class BitVectorType>
class TruncatedBlockGSStep<SumOperator<SparseMatrixType, LowRankMatrixType>, VectorType, BitVectorType>
    : public LinearIterationStep<SumOperator<SparseMatrixType, LowRankMatrixType>, VectorType, BitVectorType>
{
    typedef SumOperator<SparseMatrixType, LowRankMatrixType> MatrixType;
    typedef typename VectorType::block_type VectorBlock;
    typedef typename VectorType::field_type field_type;

    typedef typename SparseMatrixType::block_type SpBlock;
    typedef typename LowRankMatrixType::block_type LRBlock;
    typedef typename Dune::MatrixVector::Promote<SpBlock, LRBlock>::Type MBlock;
    typedef typename VectorType::block_type VBlock;

public:

    //! Default constructor.  Doesn't init anything
    TruncatedBlockGSStep(int localSolver=0):
        localSolver(localSolver)
    {}

    //! Constructor with a linear problem
    TruncatedBlockGSStep(MatrixType& mat, VectorType& x, VectorType& rhs, int localSolver=0):
        LinearIterationStep<MatrixType,VectorType>(mat, x, rhs),
        localSolver(localSolver)
    {}

    //! Perform one iteration
    virtual void iterate()
    {
        typedef typename SparseMatrixType::row_type::ConstIterator SparseMatrixColumnIterator;

        const MatrixType& mat = *this->mat_;
        const SparseMatrixType& sparse_mat = mat.sparseMatrix();
        const LowRankMatrixType& lowrank_mat = mat.lowRankMatrix();
        const VectorType& rhs = *this->rhs_;
        VectorType& x   = *this->x_;
        const BitVectorType& ignore = this->ignore();

        // compute m*x once and only add/subtract single summands in the iterations later
        VectorType mx(lowrank_mat.lowRankFactor().N());
        lowrank_mat.lowRankFactor().mv(x,mx);
        // remember entries from old iterate
        VBlock xi;

        for(typename SparseMatrixType::size_type row=0; row<sparse_mat.N(); ++row)
        {
            xi = x[row];
            VBlock r = rhs[row];
            MBlock Aii(0);

            SparseMatrixColumnIterator it = sparse_mat[row].begin();
            SparseMatrixColumnIterator end = sparse_mat[row].end();
            for(; it!=end; ++it)
            {
                size_t col = it.index();
                if (col == row)
                    Dune::MatrixVector::addProduct(Aii, 1.0, *it);
                else
                    it->mmv(x[col],r);
            }

            // lowrank specific stuff
            // add diagonal entry to Aii
            for (typename LowRankMatrixType::size_type k=0; k<lowrank_mat.lowRankFactor().N(); ++k)
            {
                const LRBlock& mki(lowrank_mat.lowRankFactor()[k][row]);
                // Aii += m^tm[i][i]
                // this should actually be \sum(lr_block^t*lr_block) if the blocks are nonsymmetric
                Dune::MatrixVector::addProduct(Aii,mki,mki);

                // r -= m^tm*x, where x[row] were zero
                mki.mmv(mx[k], r);
                VBlock temp;
                mki.mv(x[row], temp);
                mki.umv(temp,r);
            }

            // "solve" Local system by one GS step
//            if (Aii!=0)
#if 0
            {
                for(typename MBlock::size_type i=0; i<Aii.N(); ++i)
                {
                    const typename MBlock::field_type& aii = Aii[i][i];
                    if (std::abs(aii)>1e-13)
                    {
                        if (not(ignore[row][i]))
                        {
                            x[row][i] = r[i];
                            typename MBlock::row_type::Iterator inner_it = Aii[i].begin();
                            typename MBlock::row_type::Iterator inner_end = Aii[i].end();
                            for(; inner_it!=inner_end; ++inner_it)
                                if (inner_it.index()!=i)
                                    x[row][i] -= (*inner_it) * x[row][inner_it.index()];
                            x[row][i] /= aii;
                        }
                    }
                }
            }
#endif
            if (localSolver==1)
                localExactSolve(Aii, r, x[row], ignore[row]);
            else
                localGSSolve(Aii, r, x[row], ignore[row]);


            // update the mx
            for (typename LowRankMatrixType::size_type k=0; k<lowrank_mat.lowRankFactor().N(); ++k)
            {
                const LRBlock& mki(lowrank_mat.lowRankFactor()[k][row]);
                mki.umv(x[row]-xi, mx[k]);
            }
        }
    }

    private:

        int localSolver;

        /**  \brief solve method for local problems
          *
          *  This version "solves" by one GS step.
          *  This is the "default" behaviour
          */
        void localGSSolve(MBlock& A, VBlock& b, VBlock& x, typename BitVectorType::const_reference ignore)
        {
            for(typename MBlock::size_type i=0; i<A.N(); ++i)
            {
                const typename MBlock::field_type& aii = A[i][i];
                if (std::abs(aii)>1e-13)
                {
                    if (not(ignore[i]))
                    {
                        x[i] = b[i];
                        typename MBlock::row_type::Iterator inner_it = A[i].begin();
                        typename MBlock::row_type::Iterator inner_end = A[i].end();
                        for(; inner_it!=inner_end; ++inner_it)
                            if (inner_it.index()!=i)
                                x[i] -= (*inner_it) * x[inner_it.index()];
                        x[i] /= aii;
                    }
                }
            }
        }

        /**  \brief solve method for local problems
          *
          *  uses the solve method of FieldMatrix
          */
        void localExactSolve(MBlock& A, VBlock& b, VBlock& x, typename BitVectorType::const_reference ignore)
        {
            for(typename MBlock::size_type i=0; i<A.N(); ++i)
            {
                assert(A.exists(i,i)); // No implementation for matrices w/o explicit diagonal elements is available here.

                bool rowIsZero = true;
                if (ignore[i])
                {
                    A[i] = 0.0;
                }
                else
                {
                    typename MBlock::row_type::Iterator inner_it = A[i].begin();
                    typename MBlock::row_type::Iterator inner_end = A[i].end();
                    for(; inner_it!=inner_end; ++inner_it)
                        if (*inner_it != 0.0)
                        {
                            rowIsZero = false;
                            break;
                        }
                }

                if (rowIsZero)
                {
                    A[i][i] = 1.0;
                    b[i] = x[i];
                }
            }

            A.solve(x,b);

        }
};

#endif
