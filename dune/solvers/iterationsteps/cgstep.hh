// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_ITERATIONSTEPS_CGSTEP_HH
#define DUNE_SOLVERS_ITERATIONSTEPS_CGSTEP_HH

#include <memory>

#include <dune/solvers/common/preconditioner.hh>
#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/common/wrapownshare.hh>
#include <dune/solvers/iterationsteps/lineariterationstep.hh>

namespace Dune {
    namespace Solvers {

        //! A conjugate gradient solver
        template <class MatrixType, class VectorType, class Ignore = DefaultBitVector_t<VectorType>>
        class CGStep : public LinearIterationStep<MatrixType,VectorType,Ignore>
        {
            using Base = LinearIterationStep<MatrixType,VectorType,Ignore>;
            using Preconditioner = Dune::Solvers::Preconditioner<MatrixType, VectorType, Ignore>;

        public:
            CGStep() = default;

            CGStep(const MatrixType& matrix,
                   VectorType& x,
                   const VectorType& rhs)
                : Base(matrix,x), p_(rhs.size()), r_(rhs)
            {}

            template<typename P>
            CGStep(const MatrixType& matrix,
                   VectorType& x,
                   const VectorType& rhs,
                   P&& preconditioner)
                : Base(matrix,x), p_(rhs.size()), r_(rhs),
                  preconditioner_(wrap_own_share<Preconditioner>(std::forward<P>(preconditioner)))
            {}

            //! Set linear operator, solution and right hand side
            virtual void setProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs) override
            {
                this->setMatrix(mat);
                setProblem(x);
                r_ = rhs;
                p_.resize(r_.size());
            }

            template<typename P>
            void setPreconditioner(P&& preconditioner)
            {
                preconditioner_ = wrap_own_share<Preconditioner>(std::forward<P>(preconditioner));
            }

            void check() const override;
            void preprocess() override;
            void iterate() override;

            using Base::setProblem;

        private:
            VectorType p_; // search direction
            VectorType r_; // residual
            using Base::x_;
            double r_squared_old_;
            std::shared_ptr<Preconditioner> preconditioner_;
        };

    }
}

#include "cgstep.cc"

#endif
