// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_MINIMAL_POLYNOMIAL_EXTRAPOLATION_STEP_HH
#define DUNE_MINIMAL_POLYNOMIAL_EXTRAPOLATION_STEP_HH

#include <dune/common/bitsetvector.hh>

#include <dune/common/fmatrix.hh>
#include <dune/istl/matrix.hh>
#include <dune/istl/preconditioners.hh>
#include <dune/istl/solvers.hh>

#include <dune/solvers/common/preconditioner.hh>
#include <dune/solvers/iterationsteps/iterationstep.hh>

/** \brief A single step of the Minimal Polynomial Extrapolation method
 *
 * Minimal Polynomial Extrapolation (MPE) is a general method for sequence acceleration.
 * It takes a converging sequence (actually, it even works for some nonconverging ones)
 * and interpolates the iterates such as to obtain a second sequence which converges
 * faster.
 *
 * In this implementation, the original sequence is produced by an IterationStep object.
*/
template<class VectorType,
         class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension> >
         class MinimalPolynomialExtrapolationStep
         : public IterationStep<VectorType, BitVectorType>
{

public:

    /** \brief Constructor
      * \param iterationStep The iteration step object that creates the sequence being accelerated
      * \param restart Restart the method after this number of iterations
     */
    MinimalPolynomialExtrapolationStep(IterationStep<VectorType,BitVectorType>* iterationStep,
                                       int restart)
        : iterationStep_(iterationStep),
          restart_(restart)
    {}

    //! Perform one iteration
    virtual void iterate();

    /** \brief To be called before starting to iterate
       \note This calls the preprocess method for the dependent iteration step class, too!
     */
    virtual void preprocess();

    IterationStep<VectorType,BitVectorType>* iterationStep_;

    /** \brief Restart the method after this number of iterations */
    int restart_;

    /** \brief The history of iterates of the original sequence */
    std::vector<VectorType> xHistory_;

    /** \brief The history of differences between the iterates of the original sequence */
    std::vector<VectorType> U_;

};

template<class VectorType, class BitVectorType>
inline
void MinimalPolynomialExtrapolationStep<VectorType, BitVectorType>::preprocess()
{
    iterationStep_->preprocess();

    xHistory_.resize(1);
    xHistory_[0] = *this->x_;

    U_.resize(0);
}

template<class VectorType, class BitVectorType>
inline
void MinimalPolynomialExtrapolationStep<VectorType, BitVectorType>::iterate()
{
    // append a copy of the last element to the history, for the starting value
    xHistory_.push_back(xHistory_.back());

    // iterate once
    iterationStep_->x_ = &xHistory_.back();
    iterationStep_->iterate();

    //std::cout << "x[" << xHistory_.size()-1 << "]:\n" << xHistory_.back() << std::endl;

    // update the history of sequence differences
    VectorType diff = xHistory_.back();
    diff -= xHistory_[xHistory_.size()-2];
    U_.push_back(diff);

    // current number of iterates used for interpolation
    const size_t k= U_.size()-1;

    /** Compute weights c by solving the system
    * \f[ U^T_{k-1} U_{k-1} c = -U_{k-1} u_k \f]
    */
    typedef Dune::Matrix<Dune::FieldMatrix<double,1,1> > ScalarMatrixType;
    typedef Dune::BlockVector<Dune::FieldVector<double,1> > ScalarVectorType;

    // set up the matrix
    ScalarMatrixType UTU(k,k);

    for (size_t i=0; i<k; i++)
        for (size_t j=0; j<k; j++)
            UTU[i][j] = U_[i] * U_[j];

    // set up the right hand side
    ScalarVectorType rhs(k);
    for (size_t i=0; i<k; i++)
        rhs[i] = U_[i] * U_[k];
    rhs *= -1;

    // solve the system
    ScalarVectorType c(k);
    c = 0;

    // Technicality:  turn the matrix into a linear operator
    Dune::MatrixAdapter<ScalarMatrixType,ScalarVectorType,ScalarVectorType> op(UTU);

    // A preconditioner
    Dune::SeqILU<ScalarMatrixType,ScalarVectorType,ScalarVectorType> ilu0(UTU, 0, 1.0);

    // A preconditioned conjugate-gradient solver
    Dune::CGSolver<ScalarVectorType> cg(op,ilu0,1e-6,  // tolerance
                                        100,          // max number of iterations
                                        0);           // verbosity level

    // Object storing some statistics about the solving process
    Dune::InverseOperatorResult statistics;

    // Solve!
    cg.apply(c, rhs, statistics);

    // The last coefficient of the c array is '1'
    c.resize(c.size()+1, true);
    c[c.size()-1] = 1;

    //std::cout << "c:\n" << c << std::endl;

    /////////////////////////////////////////////////////////////
    //  Compute the new accelerated iterate
    /////////////////////////////////////////////////////////////
    VectorType& newIterate = *this->x_;
    newIterate = 0;
    double cSum = std::accumulate(c.begin(), c.end(), double(0));

    for (size_t i=1; i<=k+1; i++)
        newIterate.axpy(c[i-1]/cSum, xHistory_[i]);

    //std::cout << "y:\n" << newIterate << std::endl;

    // Acceleration methods for nonlinear problems should be restarted
    // every now and then
    if (k==restart_) {

        std::cout << "Restarting MPE..." << std::endl;
        U_.resize(0);
        xHistory_.resize(1);
        xHistory_[0] = *this->x_;

    }

}

#endif
