// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_ITERATIONSTEPS_ITERATIONSTEP_HH
#define DUNE_SOLVERS_ITERATIONSTEPS_ITERATIONSTEP_HH

#include <vector>
#include <string>

#include <dune/solvers/common/canignore.hh>
#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/common/numproc.hh>

namespace Dune {

namespace Solvers {

    //! Base class for iteration steps being called by an iterative solver
template<class VectorType, class BitVectorType = Solvers::DefaultBitVector_t<VectorType> >
class IterationStep : virtual public NumProc, public CanIgnore<BitVectorType>
    {
    public:
        typedef VectorType Vector;
        typedef BitVectorType BitVector;

        //! Default constructor
        IterationStep() {
          x_ = nullptr;
        }

        /** \brief Destructor */
        virtual ~IterationStep() {}

        //! Constructor being given the current iterate
        IterationStep(VectorType& x) :
            x_(&x)
        {}

        //! Set the current iterate
        virtual void setProblem(VectorType& x) {
            x_ = &x;
        }

        //! to be called before iteration
        virtual void preprocess() {}

        //! Do the actual iteration
        virtual void iterate() = 0;

        //! Access the stored pointer to iterate
        virtual const Vector* getIterate() const
        {
            return x_;
        }

        //! Access the stored pointer to iterate
        virtual Vector* getIterate()
        {
            return x_;
        }

        //! Return solution object
        virtual VectorType getSol()
        {
            return *getIterate();
        }

        /** \brief Checks whether all relevant member variables are set
         * \exception SolverError if the iteration step is not set up properly
         */
        virtual void check() const {
#if 0
            if (!x_)
                DUNE_THROW(SolverError, "Iteration step has no solution vector");
#endif
        }

        //! get output of last iteration step
        virtual std::string getOutput() const
        {
            return "";
        }

        //! The solution container
        VectorType* x_;
    };

}  // namespace Solvers

}  // namespace Dune

// For backward compatibility: will be removed eventually

using Dune::Solvers::IterationStep;

#endif
