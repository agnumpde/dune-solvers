// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef PRECONDITIONER_HH
#define PRECONDITIONER_HH

#include <dune/common/bitsetvector.hh>

#include <dune/solvers/common/numproc.hh>

/** \brief Abstract base class for preconditioners
 */
template <class VectorType>
class Preconditioner
   : public NumProc
{
public:

    /** \brief Virtual destructor.  */
    virtual ~Preconditioner()
    {}

    /** \brief Apply the preconditioner: compute the residual for a given iterate
     */
    virtual void apply(const VectorType& x, VectorType& r) const = 0;
    
};

namespace Dune {
    namespace Solvers {

        /** \brief Abstract base class for preconditioners
         */
        template <class MatrixType, class VectorType, class BitVectorType
                  = Dune::BitSetVector<VectorType::block_type::dimension> >
        class Preconditioner : virtual public NumProc
        {
        public:
            virtual ~Preconditioner() {}

            virtual void check() const {}
            virtual void setMatrix(const MatrixType& matrix) = 0;
            virtual void apply(VectorType& x, const VectorType& r) = 0;
        };
    }
}
#endif
