// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_SOLVERS_COMMON_TUPLEVECTOR_HH
#define DUNE_SOLVERS_COMMON_TUPLEVECTOR_HH

#warning This file is deprecated!  Use tuplevector.hh from the dune-common module instead!

#include <dune/common/tuplevector.hh>

namespace Dune
{
namespace Solvers
{

  /**
   * \brief A std::tuple that allows access to its element via operator[]
   */
  template<class... T>
  using TupleVector = Dune::TupleVector<T...>;

}  // namespace Functions

}  // namespace Dune

#endif
