// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_COMMON_CANIGNORE_HH
#define DUNE_SOLVERS_COMMON_CANIGNORE_HH

#include <cassert>

/** \brief Abstract base class for solvers and iterationsteps that are
 *      able to ignore degrees of freedom described by a bit field.
 */
template <class BitVectorType>
class CanIgnore
{
public:

    using BitVector = BitVectorType;

    /**
     * \brief Default constructor
     */
    CanIgnore() = default;

    /**
     * \brief Constructor from bit vector
     *
     * This class stores a non-owning pointer to the bit vector.
     */
    CanIgnore(const BitVector& i)
        : ignoreNodes_(&i)
    {}

    /**
     * \brief Destructor
     *
     * Does NOT delete the bitfield!
     */
    ~CanIgnore() = default;

    /**
     * \brief Set bit vector
     *
     * This class stores a non-owning pointer to the bit vector.
     */
    void setIgnore(const BitVector& i)
    {
        ignoreNodes_ = &i;
    }

    /**
     * \brief Only if this returns true can ignore() safely be called.
     */
    bool hasIgnore() const {
        return ignoreNodes_ != nullptr;
    }

    /**
     * \brief Const access to bit vector
     */
    const BitVector& ignore() const
    {
        assert(hasIgnore());
        return *ignoreNodes_;
    }

    /**
     * \brief A flag for each degree of freedom stating whether the dof should be ignored by the solver
     */
    const BitVectorType* ignoreNodes_ = nullptr;

};

#endif // DUNE_SOLVERS_COMMON_CANIGNORE_HH
