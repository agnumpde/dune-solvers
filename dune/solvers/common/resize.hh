// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifndef DUNE_SOLVERS_COMMON_RESIZE_HH
#define DUNE_SOLVERS_COMMON_RESIZE_HH

#include <dune/common/indices.hh>
#include <dune/common/concept.hh>
#include <dune/common/typetraits.hh>

#include <dune/solvers/common/defaultbitvector.hh>



namespace Dune {
namespace Solvers {

namespace Concept {

struct HasResize
{
  template<class C>
  auto require(C&& c) -> decltype(
    c.resize(0)
  );
};

} // namespace Concept



namespace Impl {

template<int size, class Alloc, class Vector>
void resizeInitialize(Dune::BitSetVector<size, Alloc>& x, const Vector& y, bool value)
{
  x.resize(y.size());
  if (value)
    x.setAll();
  else
    x.unsetAll();
}

template<class TargetVector, class Vector, class Value,
  std::enable_if_t<not Dune::IsIndexable<TargetVector, std::integral_constant<std::size_t,0> >::value, int> = 0>
void resizeInitialize(TargetVector& x, const Vector& y, const Value& value)
{
  x = value;
}

template<class TargetVector, class Vector, class Value,
  std::enable_if_t<Dune::IsIndexable<TargetVector, std::integral_constant<std::size_t,0> >::value, int> = 0>
void resizeInitialize(TargetVector& x, const Vector& y, const Value& value)
{
  namespace H = Dune::Hybrid;
  auto size = H::size(y);

  H::ifElse(models<Concept::HasResize, TargetVector>(), [&](auto&& id) {
      id(x).resize(size);
  }, [&](auto&& id) {
    if (H::size(x) != size)
      DUNE_THROW(RangeError, "Can't resize statically sized vector of size " << H::size(x) << " to size " << size);
  });
  H::forEach(H::integralRange(size), [&](auto&& i) {
    resizeInitialize(x[i], y[i], value);
  });
}

} // namespace Impl



/**
 * \brief Resize and initialization vector to match size of given vector
 *
 * \param x Vector to resize
 * \param y Model for resizing
 * \param value Value to use for initialization
 *
 * This will resize the given vector x to match
 * the size of the given vector and assign the given
 * value to it.
 */
template<class TargetVector, class Vector, class Value>
void resizeInitialize(TargetVector& x, const Vector& y, Value&& value)
{
  Impl::resizeInitialize(x, y, value);
}

/**
 * \brief Resize and initialization vector to match size of given vector
 *
 * \param x Vector to resize
 * \param y Model for resizing
 *
 * This will resize the given vector x to match
 * the size of the given vector and initialize
 * all entries with zero.
 */
template<class TargetVector, class Vector>
void resizeInitializeZero(TargetVector& x, const Vector& y)
{
    resizeInitialize(x, y, 0);
}





} // end namespace Solvers
} // end namespace Dune



#endif // DUNE_SOLVERS_COMMON_RESIZE_HH
