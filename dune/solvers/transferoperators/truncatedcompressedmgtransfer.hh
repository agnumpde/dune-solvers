// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef TRUNCATED_COMPRESSED_MG_TRANSFER_HH
#define TRUNCATED_COMPRESSED_MG_TRANSFER_HH

#include <dune/istl/bcrsmatrix.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/bitsetvector.hh>

#include "truncatedmgtransfer.hh"
#include "compressedmultigridtransfer.hh"


/** \brief Restriction and prolongation operator for truncated multigrid
 *
 * This class provides prolongation and restriction operators for truncated
 * multigrid.  That means that you can explicitly switch off certain
 * fine grid degrees-of-freedom.  This often leads to better coarse
 * grid corrections when treating obstacle problems.  For an introduction
 * to the theory of truncated multigrid see 'Adaptive Monotone Multigrid
 * Methods for Nonlinear Variational Problems' by R. Kornhuber.
 *
 * Currently only works for first-order Lagrangian elements!
 */

template<
    class VectorType,
    class BitVectorType = Dune::BitSetVector<VectorType::block_type::dimension>,
    class MatrixType = Dune::BCRSMatrix< typename Dune::FieldMatrix<
        typename VectorType::field_type, VectorType::block_type::dimension, VectorType::block_type::dimension> > >
class TruncatedCompressedMGTransfer :
    public CompressedMultigridTransfer<VectorType, BitVectorType, MatrixType>,
    public TruncatedMGTransfer<VectorType, BitVectorType, MatrixType>
{

    enum {blocksize = VectorType::block_type::dimension};

    typedef typename VectorType::field_type field_type;
    typedef CompressedMultigridTransfer<VectorType, BitVectorType, MatrixType> Base;

public:

    typedef typename Base::TransferOperatorType TransferOperatorType;

    /** \brief Default constructor */
    using Base::Base;

    /** \brief Import method for restricting bit fields */
    using Base::restrict;

    /** \brief Restrict level fL of f and store the result in level cL of t */
    void restrict(const VectorType& f, VectorType &t) const override;

    /** \brief Prolong level cL of f and store the result in level fL of t */
    void prolong(const VectorType& f, VectorType &t) const override;

    /** \brief Galerkin assemble a coarse stiffness matrix */
    void galerkinRestrict(const MatrixType& fineMat, MatrixType& coarseMat) const override;
};


#include "truncatedcompressedmgtransfer.cc"

#endif
