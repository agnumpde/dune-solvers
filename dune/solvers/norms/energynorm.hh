// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_NORMS_ENERGY_NORM_HH
#define DUNE_SOLVERS_NORMS_ENERGY_NORM_HH

#include <cmath>
#include <functional>

#include <dune/matrix-vector/axy.hh>

#include <dune/solvers/norms/norm.hh>
#include <dune/solvers/iterationsteps/lineariterationstep.hh>

namespace Dune {
namespace Solvers {

    /** \brief Vector norm induced by linear operator
     *
     *  \f$\Vert u \Vert_A = (u, Au)^{1/2}\f$
     *
     * As alternative to providing the EnergyNorm directly with a matrix
     * one can also provide it with a LinearIterationStep. In this case
     * the matrix for the linear problem associated with the LinearIterationStep
     * is used. This is necessary because you sometimes do not
     * know the matrix in advance. This is, for example, the case
     * for the coarse level matrices constructed by a multilevel solver.
     *
     * Be careful: This matrix is not the one representing the preconditioner
     * induced by the LinearIterationStep.
     *
     *  \todo Elaborate documentation.
     */
    template<class MatrixType, class V>
    class EnergyNorm : public Norm<V>
    {
    public:
        typedef V VectorType;
        using Base = Norm<V>;

        /** \brief The type used for the result */
        using typename Base::field_type;

        EnergyNorm(const field_type tol=1e-10 ) : tol_(tol) {}

        template<class BV>
        EnergyNorm(LinearIterationStep<MatrixType, VectorType, BV>& it, const field_type tol=1e-10)
            : EnergyNorm(tol)
        {
            setIterationStep(&it);
        }

        EnergyNorm(const MatrixType& matrix, const field_type tol=1e-10)
            : EnergyNorm(tol)
        {
            setMatrix(&matrix);
        }

        //! \brief sets the energy norm matrix
        void setMatrix(const MatrixType* matrix) {
            matrixProvider_ = [=]() -> const MatrixType& { return *matrix; };
        }

        //! \brief get the energy norm matrix
        const MatrixType& getMatrix() const {
            return matrixProvider_();
        }

        //! \brief sets to use the current problem matrix of the linear iteration step
        template<class BV>
        void setIterationStep(LinearIterationStep<MatrixType, VectorType, BV>* step) {
            matrixProvider_ = [=]() -> const MatrixType& { return *step->getMatrix(); };
        }

        //! Compute the norm of the difference of two vectors
        field_type diff(const VectorType& f1, const VectorType& f2) const override {
            VectorType tmp_f = f1;
            tmp_f -= f2;
            return (*this)(tmp_f);
        }

        //! Compute the norm of the given vector
        field_type operator()(const VectorType& f) const override
        {
            return std::sqrt(normSquared(f));
        }

        // \brief Compute the square of the norm of the given vector
        virtual field_type normSquared(const VectorType& f) const override
        {
            if (not matrixProvider_)
                DUNE_THROW(Dune::Exception, "You have supplied neither a matrix nor an IterationStep to the EnergyNorm!");
            const field_type ret = Dune::MatrixVector::Axy(matrixProvider_(), f, f);
            return checkedValue(ret, tol_);
        }

        // \brief Compute the squared norm for a given vector and matrix
        [[deprecated]] static field_type normSquared(const VectorType& u,
                                                      const MatrixType& A,
                                                      const field_type tol=1e-10)
        {
            const field_type ret = Dune::MatrixVector::Axy(A, u, u);
            return checkedValue(ret, tol);
        }

    protected:

        //! yields access to energy matrix
        std::function<const MatrixType&()> matrixProvider_;

        const field_type tol_;

        //! \brief throw an exception if value is below tolerance, project to R^+ otherwise.
        static field_type checkedValue(field_type value, field_type tolerance)
        {
            if (value < 0) {
                if (value < -tolerance)
                    DUNE_THROW(Dune::RangeError, "Supplied linear operator is not positive (semi-)definite: (u,Au) = " << value);
                else return 0.0;
            }
            return value;
        }
    };

} /* namespace Solvers */
} /* namespace Dune */

// For backward compatibility: will be removed eventually
using Dune::Solvers::EnergyNorm;

#endif
