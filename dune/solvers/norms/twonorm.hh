// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef TWONORM_HH
#define TWONORM_HH

#include <cmath>
#include <cstring> // For size_t

#include <dune/common/fvector.hh>
#include <dune/common/hybridutilities.hh>

#include "norm.hh"

//! Wrapper around the two_norm() method of a vector class
template <class V>
class TwoNorm : public Norm<V>
{
    public:
       typedef V VectorType;
       using Base = Norm<V>;

       /** \brief The type used for the result */
       using typename Base::field_type;

        /** \brief Destructor, doing nothing */
        virtual ~TwoNorm() {}

        //! Compute the norm of the given vector
        virtual field_type operator()(const VectorType& f) const override
        {
            return f.two_norm();
        }

        //! Compute the square of the norm of the given vector
        virtual field_type normSquared(const VectorType& f) const override
        {
            return f.two_norm2();
        }

        // TODO there is no reason to have this if f.two_norm2() can be
        //      assumed (above) anyway. Then also the specialization for
        //      FieldVector (below) can go.
        //! Compute the norm of the difference of two vectors
        virtual field_type diff(const VectorType& f1, const VectorType& f2) const override
        {
            assert(f1.size() == f2.size());
            field_type r = 0.0;

            Dune::Hybrid::forEach(Dune::Hybrid::integralRange(Dune::Hybrid::size(f1)), [&](auto&& i)
            {
                auto block = f1[i];
                block -= f2[i];

                r += Dune::Impl::asVector(block).two_norm2();
            });

            return std::sqrt(r);
        }

};

template <typename T, int dimension>
class TwoNorm<Dune::FieldVector<T, dimension> >
  : public Norm<Dune::FieldVector<T, dimension> >
{
    typedef Dune::FieldVector<T, dimension> VectorType;

    public:

        /** \brief Destructor, doing nothing */
        virtual ~TwoNorm() {};

        //! Compute the norm of the given vector
        virtual T operator()(const VectorType& f) const override
        {
            return f.two_norm();
        }

        //! Compute the square of the norm of the given vector
        virtual T normSquared(const VectorType& f) const override
        {
            return f.two_norm2();
        }

        //! Compute the norm of the difference of two vectors
        virtual T diff(const VectorType& f1, const VectorType& f2) const override
        {
          VectorType tmp = f1;
          tmp -= f2;
          return tmp.two_norm();
        }
};

#endif
