# Master (will become release 2.11)

- ...

# Release 2.10

- Deprecate the file `tuplevector.hh`.  An equivalent file exists in `dune-common`
  since 2016. Please use that from now on.

- `UMFPackSolver` accepts each correctly formed blocked matrix. The user has to make sure that the vector types of `x` and `rhs` are compatible to the matrix.
  The main advantage is that it is now possible to use `MultiTypeBlockMatrix`.

- A new solver `ProximalNewtonSolver` is added which solves non-smooth minimization problems.

# Release 2.9

- The internal matrix of the`EnergyNorm` can now be accessed by `getMatrix()`.

- The default `BitVectorType` of the class `IterationStep` is now
  `Solvers::DefaultBitVector_t<VectorType>` rather than `Dune::BitSetVector`.
  This should do the right thing in more situations, while being fully
  backward-compatible.

- `codespell` spell checker is now active for automated spell checking in the Gitlab CI. 
  To exclude false positives add the words to the `--ignore-words-list` in `.gitlab-ci.yml`.

# Release 2.8

- `UMFPackSolver` can now handle matrices and vectors with scalar entries.

- CholmodSolver: Added `errorCode_` member to report errors and warnings during the matrix decomposition

- CholmodSolver: `CholmodSolver` can be used with any blocked matrix/vector type supported by `flatMatrixForEach` and `flatVectorForEach` in dune-istl.

- CholmodSolver: There are now various new methods that allow to factorize the matrix once,
  and use the factorization to solve linear systems with several right hand sides.

## Deprecations and removals

- The file `blockgsstep.hh` has been removed after four years of deprecation.  Use the replacement
  in `blockgssteps.hh` instead.
